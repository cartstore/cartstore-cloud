<?php

define('CANADA_POST_DOM_RP','Regular Parcel');
define('CANADA_POST_DOM_EP','Expedited Parcel');
define('CANADA_POST_DOM_XP','Xpresspost');
define('CANADA_POST_DOM_XP_CERT','Xpresspost Certified');
define('CANADA_POST_DOM_PC','Priority');
define('CANADA_POST_DOM_LIB','Library Books');
define('CANADA_POST_USA_EP','Expedited Parcel USA');
define('CANADA_POST_USA_PW_ENV','Priority Worldwide Envelope USA');
define('CANADA_POST_USA_PW_PAK','Priority Worldwide pak USA');
define('CANADA_POST_USA_PW_PARCEL','Priority Worldwide Parcel USA');
define('CANADA_POST_USA_SP_AIR','Small Packet USA Air');
define('CANADA_POST_USA_SP_SURF','Small Packet USA Surface');
define('CANADA_POST_USA_XP','Xpresspost USA');
define('CANADA_POST_INT_XP','Xpresspost International');
define('CANADA_POST_INT_IP_AIR','International Parcel Air');
define('CANADA_POST_INT_IP_SURF','International Parcel Surface');
define('CANADA_POST_INT_PW_ENV','Priority Worldwide Envelope International');
define('CANADA_POST_INT_PW_PAK','Priority Worldwide pak International');
define('CANADA_POST_INT_PW_PARCEL','Priority Worldwide parcel International');
define('CANADA_POST_INT_SP_AIR','Small Packet International Air');
define('CANADA_POST_INT_SP_SURF','Small Packet International Surface');
define('MODULE_SHIPPING_CANADAPOST_TEXT_UNKNOWN_ERROR','unknown error from Canada Post');
define('MODULE_SHIPPING_CANADA_POST_TEXT_TITLE','Canada Post Service');
define('MODULE_SHIPPING_CANADA_POST_TEXT_DESCRIPTION','Canada Post Rating Service (REST)<hr />You will need to have API Keys username and password, along with your Canada Post customer number to use this module<br /><br />defails please visit <a href="http://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/sellonline/default.jsf">www.canadapost.ca</a> ');
?>