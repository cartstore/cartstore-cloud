<?php
/*
    $Id: canadapost.php,v 0.0.1 2012/10/10 JanZ Exp $
    Original copyright (c) 2012 Sean Zhuang
    Written by Sean Zhuang, sean.ontario@gmail.com
*/

class canadapost {
    var $code, $title, $description, $icon, $enabled, $types, $boxcount;

    //***************
    function canadapost() {
        global $order, $packing;
        $this->code = 'canadapost';
        $this->title = MODULE_SHIPPING_CANADA_POST_TEXT_TITLE;
        $this->description = MODULE_SHIPPING_CANADA_POST_TEXT_DESCRIPTION;
        $this->sort_order = MODULE_SHIPPING_CANADA_POST_SORT_ORDER;
        $this->icon = DIR_WS_ICONS . 'shipping_canadapost_small.gif';
        $this->tax_class = MODULE_SHIPPING_CANADA_POST_TAX_CLASS;
        $this->enabled = ((MODULE_SHIPPING_CANADA_POST_STATUS == 'True') ? true : false);
        $this->access_username = MODULE_SHIPPING_CANADA_POST_USERNAME;
        $this->access_password = MODULE_SHIPPING_CANADA_POST_PASSWORD;
        $this->access_customer_number = MODULE_SHIPPING_CANADA_POST_CUSTOMER_NUMBER;
		$this->timeout = '60';
		$this->quotetype = MODULE_SHIPPING_CANADA_POST_QUOTE_TYPE;
		$this->contract_id = MODULE_SHIPPING_CANADA_POST_CONTRACT_ID;
        $this->origin_postalcode = MODULE_SHIPPING_CANADA_POST_POSTALCODE;
		$this->use_insurance =((MODULE_SHIPPING_CANADA_POST_INSURE == 'True') ? true : false);

// the variables for unit weight, unit length, and dimensions support were moved to
// shop admin -> Configuration -> Shipping/Packaging in
// Run the configuration_shipping.sql to add these to your configuration
        if (defined('SHIPPING_UNIT_WEIGHT')) {
          $this->unit_weight = SHIPPING_UNIT_WEIGHT;
        } else {
// for those who will undoubtedly forget or not know how to run the configuration_shipping.sql
// we will set the default to pounds (LBS) and inches (IN)
          $this->unit_weight = 'LBS';
        }
        if (defined('SHIPPING_UNIT_LENGTH')) {
          $this->unit_length = SHIPPING_UNIT_LENGTH;
        } else {
          $this->unit_length = 'IN';
        }
		
        if (defined('SHIPPING_DIMENSIONS_SUPPORT') && SHIPPING_DIMENSIONS_SUPPORT == 'Ready-to-ship only') {
          $this->dimensions_support = 0;
        } elseif (defined('SHIPPING_DIMENSIONS_SUPPORT') && SHIPPING_DIMENSIONS_SUPPORT == 'With product dimensions') {
          $this->dimensions_support = 0;
        } else {
          $this->dimensions_support = 0;
        }
        $this->protocol = 'https';
        $this->host = ((MODULE_SHIPPING_CANADA_POST_MODE == 'Test') ? 'ct.soa-gw.canadapost.ca' : 'soa-gw.canadapost.ca');
        $this->port = '443';
        $this->path = '/rs/ship/price';

        $this->items_qty = 0;

        // insurance addition
        $this->insure_package = false;
        if (MODULE_SHIPPING_CANADA_POST_INSURE == 'True') { 
          $this->insure_package = true;
        }
        // end insurance addition
        $this->pkgvalue = ceil($order->info['subtotal']); // is divided by number of boxes later

    } // end function upsxml

    //**************
    function check() {
        if (!isset($this->_check)) {
            $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_CANADA_POST_STATUS'");
            $this->_check = tep_db_num_rows($check_query);
        }
        return $this->_check;
    }

    //****************
    function remove() {
        tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function install() {
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Canada Post Shipping Service(REST)', 'MODULE_SHIPPING_CANADA_POST_STATUS', 'True', 'Do you want to offer Canada Post shipping?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Canada Post Customer Number', 'MODULE_SHIPPING_CANADA_POST_CUSTOMER_NUMBER', '', 'Enter the Your Canada Post Customer Number.', '6', '1', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Canada Post API Username', 'MODULE_SHIPPING_CANADA_POST_USERNAME', '', 'Enter your Canada Post (API Keys) username.', '6', '2', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Canada Post API Password', 'MODULE_SHIPPING_CANADA_POST_PASSWORD', '', 'Enter your Canada Post (API Keys) password.', '6', '3', now())");

        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Canada Post Contract ID', 'MODULE_SHIPPING_CANADA_POST_CONTRACT_ID', '', 'Enter your Canada Post contract id (for commercial/contracted rates only).', '6', '4', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Canada Post Quote Type', 'MODULE_SHIPPING_CANADA_POST_QUOTE_TYPE', 'commercial', 'Either commercial (for the contract holder) or counter (regular price for consumers)', '6', '5', 'tep_cfg_select_option(array(\'commercial\', \'counter\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Insurance', 'MODULE_SHIPPING_CANADA_POST_INSURE', 'True', 'Do you want to insure packages shipped by Canada Post?', '6', '6', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Origin City', 'MODULE_SHIPPING_CANADA_POST_CITY', '', 'Enter the name of the origin city.', '6', '7', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Origin State/Province', 'MODULE_SHIPPING_CANADA_POST_STATEPROV', '', 'Enter the two-letter code for your origin state/province.', '6', '8', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Origin Country', 'MODULE_SHIPPING_CANADA_POST_COUNTRY', '', 'Enter the two-letter code for your origin country.', '6', '9', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Origin Zip/Postal Code', 'MODULE_SHIPPING_CANADA_POST_POSTALCODE', '', 'Enter your origin zip/postalcode (from which the parcel will be sent).', '6', '10', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Test or Production Mode', 'MODULE_SHIPPING_CANADA_POST_MODE', 'Test', 'Use this module in Test  or Production mode? <br /><br />Your API Keys username and password are different in Test and Production mode', '6', '11', 'tep_cfg_select_option(array(\'Test\', \'Production\'), ', now())");

        tep_db_query("insert into " . TABLE_CONFIGURATION . " ( configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Disallowed Shipping Methods', 'MODULE_SHIPPING_CANADA_POST_TYPES', '', 'Select the Canada Post services <span style=\'color: red; font-weight: bold\'>not</span> to be offered.', '6', '12', 'get_multioption_canadapost_xml',  'canadapost_cfg_select_multioption(array(\'D.RP\',\'D.EP\',\'D.XP\',\'D.XP.CERT\',\'D.PC\',\'D.LIB\',\'U.EP\',\'U.PW.ENV\',\'U.PW.PAK\',\'U.PW.PARCEL\',\'U.SP.AIR\',\'U.SP.SURF\',\'U.XP\',\'I.XP\',\'I.IP.AIR\',\'I.IP.SURF\',\'I.PW.ENV\', \'I.PW.PAK\',\'I.PW.PARCEL\',\'I.SP.AIR\',\'I.SP.SURF\'),',  now())");

        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Handling Type', 'MODULE_SHIPPING_CANADA_POST_HANDLING_TYPE', 'Flat Fee', 'Handling type for this shipping method.', '6', '13', 'tep_cfg_select_option(array(\'Flat Fee\', \'Percentage\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Handling Fee', 'MODULE_SHIPPING_CANADA_POST_HANDLING', '0', 'Handling fee for this shipping method.', '6', '14', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_CANADA_POST_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '15', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort order of display.', 'MODULE_SHIPPING_CANADA_POST_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '16', now())");

        // add key for enabling email error messages
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Email Canada Post errors', 'MODULE_SHIPPING_CANADA_POST_EMAIL_ERRORS', 'Yes', 'Do you want to receive Canada Post errors by email?', '6', '17', 'tep_cfg_select_option(array(\'Yes\', \'No\'), ', now())");
    }

    //*************
    function keys() {
        return array('MODULE_SHIPPING_CANADA_POST_STATUS','MODULE_SHIPPING_CANADA_POST_CUSTOMER_NUMBER','MODULE_SHIPPING_CANADA_POST_USERNAME',
'MODULE_SHIPPING_CANADA_POST_PASSWORD','MODULE_SHIPPING_CANADA_POST_CONTRACT_ID','MODULE_SHIPPING_CANADA_POST_QUOTE_TYPE',
'MODULE_SHIPPING_CANADA_POST_INSURE','MODULE_SHIPPING_CANADA_POST_CITY','MODULE_SHIPPING_CANADA_POST_STATEPROV','MODULE_SHIPPING_CANADA_POST_COUNTRY',
'MODULE_SHIPPING_CANADA_POST_POSTALCODE','MODULE_SHIPPING_CANADA_POST_MODE','MODULE_SHIPPING_CANADA_POST_TYPES','MODULE_SHIPPING_CANADA_POST_HANDLING_TYPE',
'MODULE_SHIPPING_CANADA_POST_HANDLING','MODULE_SHIPPING_CANADA_POST_TAX_CLASS','MODULE_SHIPPING_CANADA_POST_SORT_ORDER','MODULE_SHIPPING_CANADA_POST_EMAIL_ERRORS');
    }

//**********************************************
    function _canadaPostOrigin($city, $stateprov, $country, $postal){
        $this->_canadaPostOriginCity = $city;
        $this->_canadaPostOriginStateProv = $stateprov;
        $this->_canadaPostOriginCountryCode = $country;
        $postal = str_replace(' ', '', $postal);
        if ($country == 'US') {
            $this->_canadaPostOriginPostalCode = substr($postal, 0, 5);
        } else {
            $this->_canadaPostOriginPostalCode = strtoupper($postal);
        }
    }

    //**********************************************
    function _canadaPostDest($city, $stateprov, $country, $postal) {
        $this->_canadaPostDestCity = $city;
        $this->_canadaPostDestStateProv = $stateprov;
        $this->_canadaPostDestCountryCode = $country;
        $postal = str_replace(' ', '', $postal);
        if ($country == 'US') {
            $this->_canadaPostDestPostalCode = substr($postal, 0, 5);
        } else if ($country == 'BR') {
            $this->_canadaPostDestPostalCode = substr($postal, 0, 5);
        } else if( $country == 'CA') {
            $this->_canadaPostDestPostalCode = strtoupper($postal);
        } else {
			$this->_canadaPostDestPostalCode = $postal;
		}
    }
	//********************************************
    function _addItem($length, $width, $height, $weight, $price = 0 ) {
        // Add box or item to shipment list. Round weights to 1 decimal places.
        if ((float)$weight < 1.0) {
            $weight = 1;
        } else {
            $weight = round($weight, 3);
        }
        $index = $this->items_qty;
        $this->item_length[$index] = ($length ? (string)round((float)$length,1) : '0' );
        $this->item_width[$index] = ($width ? (string)round((float)$width,1) : '0' );
        $this->item_height[$index] = ($height ? (string)round((float)$height,1) : '0' );
        $this->item_weight[$index] = ($weight ? (string)round((float)$weight,1) : '0' );
        $this->item_price[$index] = $price;
        $this->items_qty++;
    }
/************************************************************************************************************************************************************************************/
    // class methods
    function quote($method = '') {
        global $HTTP_POST_VARS, $order, $shipping_weight, $shipping_num_boxes, $total_weight, $boxcount, $cart, $packing;

        $state = $order->delivery['state'];
        $zone_query = tep_db_query("select zone_code from " . TABLE_ZONES . " where zone_name = '" .  tep_db_input($order->delivery['state']) . "' and zone_country_id = '" . $order->delivery['country']['id'] . "'");
        if (tep_db_num_rows($zone_query)) {
            $zone = tep_db_fetch_array($zone_query);
            $state = $zone['zone_code'];
        }
        $this->_canadaPostOrigin(MODULE_SHIPPING_CANADA_POST_CITY, MODULE_SHIPPING_CANADA_POST_STATEPROV, MODULE_SHIPPING_CANADA_POST_COUNTRY, MODULE_SHIPPING_CANADA_POST_POSTALCODE);
        $this->_canadaPostDest($order->delivery['city'], $state, $order->delivery['country']['iso_code_2'], $order->delivery['postcode']);
        
// the check on $packing being an object will puzzle people who do things wrong (no changes when 
// you enable dimensional support without changing checkout_shipping.php) but better be safe
        if ($this->dimensions_support > 0 && is_object($packing)) {
          $boxValue = 0;
          $totalWeight = $packing->getTotalWeight();
          $boxesToShip = $packing->getPackedBoxes();
          for ($i = 0; $i < count($boxesToShip); $i++) {
            $this->_addItem($boxesToShip[$i]['item_length'], $boxesToShip[$i]['item_width'], $boxesToShip[$i]['item_height'], $boxesToShip[$i]['item_weight'], $boxesToShip[$i]['item_price']);
          } // end for ($i = 0; $i < count($boxesToShip); $i++)
        } else {
            // The old method. Let osCommerce tell us how many boxes, plus the weight of each (or total? - might be sw/num boxes)
          $this->items_qty = 0; //reset quantities
// $this->pkgvalue has been set as order subtotal around line 108, it will cause overcharging
// of insurance if not divided by the number of boxes
          for ($i = 0; $i < $shipping_num_boxes; $i++) {
            $this->_addItem(0, 0, 0, $shipping_weight, number_format(($this->pkgvalue/$shipping_num_boxes), 2, '.', ''));
          }
        }

        $canadaPostQuote = $this->_canadaPostGetQuote();
		
        if ((is_array($canadaPostQuote)) && (sizeof($canadaPostQuote) > 0)) {
            $this->quotes = array('id' => $this->code, 'module' => $this->title);
            usort($canadaPostQuote, array($this, "rate_sort_func"));
           
            $methods = array();

            for ($i=0; $i < sizeof($canadaPostQuote); $i++) {
                //list($type, $cost) = each($canadaPostQuote[$i]);
				$basetype = $canadaPostQuote[$i]['service-code'];
				$cost = $canadaPostQuote[$i]['due'];
				$type = $canadaPostQuote[$i]['service-name'];

                if ($this->exclude_choices($basetype)) continue;
				
				$_type = $type;
				// changed to make handling percentage based
				if ($this->handling_type == "Percentage") {


					if ($_type) $methods[] = array('id' => $basetype, 'title' => $_type, 'cost' => (((MODULE_SHIPPING_CANADA_POST_HANDLING * $cost)/100) + $cost));
				} else {


					if ($_type) $methods[] = array('id' => $basetype, 'title' => $_type, 'cost' => (MODULE_SHIPPING_CANADA_POST_HANDLING + $cost));
				}
            }
            if ($this->tax_class > 0) {
                $this->quotes['tax'] = tep_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
            }
            $this->quotes['methods'] = $methods;
        } else {
            if ( $canadaPostQuote != false ) {
                $errmsg = $canadaPostQuote;
            } else {
                $errmsg = MODULE_SHIPPING_CANADAPOST_TEXT_UNKNOWN_ERROR;
            }
            $errmsg .= '<br>'. STORE_NAME.' via <a href="mailto:'.STORE_OWNER_EMAIL_ADDRESS.'"><u>Email</u></a>.';
            $this->quotes = array('module' => $this->title, 'error' => $errmsg);
        }
        if (tep_not_null($this->icon)) {
            $this->quotes['icon'] = tep_image($this->icon, $this->title);
        }
        return $this->quotes;
    }

	//*********************
    function _canadaPostGetQuote() {
		//need to loop throught all items or packages
		$capost= array();
		
		//first item/package
		for ($i=0; $i<sizeof($this->item_width); $i++) {
    		$xmlRequest = $this->_GetRequestXml(0);
			$result = $this->_post($xmlRequest);
			if($result===false) return false;
			$box = $this->_parseResult($result);
			if($box===false) return false;
			for($i3=0; $i3 < sizeof($box);$i3++) {
			   $merge = true;	
			   for($i2=0; $i2 < sizeof($capost);$i2++) {
				 if (strcmp($capost[$i2]['service-code'],$box[$i3]['service-code'])==0)
				 {
					$capost[$i2]['due'] += $box[$i3]['due'];
					$merge=false;
					break;
				 }
			   }	
			   if($merge) $capost[] = array('service-name'=>$box[$i3]['service-name'],'due'=>$box[$i3]['due'],'service-code' => $box[$i3]['service-code'],'expected-delivery-date'=> $box[$i3]['expected-delivery-date']);
			}
		}
        return $capost;
    }
     //*****************************
    function _parseResult($xmlResult) {
        $aryProducts = false;
        $upstemp = array();
		$priceQuotes = $xmlResult->{'price-quotes'}->children('http://www.canadapost.ca/ws/ship/rate');
		if ( $priceQuotes->{'price-quote'} ) {
			$aryProducts=array();
			foreach ( $priceQuotes as $priceQuote ) {
				$title  =(string) $priceQuote->{'service-name'};
				$code  = (string)$priceQuote->{'service-code'};
				$charge = (float) $priceQuote->{'price-details'}->{'due'};
				$ddate = (string) $priceQuote->{'service-standard'}->{'expected-delivery-date'}; //2011-09-21*/
				$aryProducts[] = array('service-name'=>$title,'due'=>$charge,'service-code' => $code,'expected-delivery-date'=> $ddate);
			}
		}
        return $aryProducts;
    }
    
    //******************************************************************
    function _post($xmlRequest) {
        $url = $this->protocol."://".$this->host.":".$this->port.$this->path;

		$curl = curl_init($url); // Create REST Request
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_CAINFO, realpath(dirname($argv[0])) . '/includes/modules/shipping/canadapost/cacert.pem'); // Signer Certificate in PEM format
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
		curl_setopt($curl, CURLOPT_TIMEOUT, (int)$this->timeout);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, $this->access_username . ':' . $this->access_password);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/vnd.cpc.ship.rate+xml', 'Accept: application/vnd.cpc.ship.rate+xml'));
		$curl_response = curl_exec($curl); // Execute REST Request
	
		// send email if enabled in the admin section
		if (curl_errno($curl) && $this->email_errors) {
			$error_from_curl = sprintf('Error [%d]: %s', curl_errno($curl), curl_error($curl));
			error_log("Error from cURL: " . $error_from_curl . " experienced by customer with id " . $_SESSION['customer_id'] . " on " . date('Y-m-d H:i:s'), 1, STORE_OWNER_EMAIL_ADDRESS);
		}
		
		curl_close ($curl);
	
		libxml_use_internal_errors(true);
		
		$xml = simplexml_load_string('<root>' . preg_replace('/<\?xml.*\?>/','',$curl_response) . '</root>');
		if (!$xml) {
			$error_from_curl = 'Failed loading XML' . "\n". $curl_response . "\n";
			if($this->email_errors) error_log("Error from cURL: " . $error_from_curl . " experienced by customer with id " . $_SESSION['customer_id'] . " on " . date('Y-m-d H:i:s'), 1, STORE_OWNER_EMAIL_ADDRESS);
		} else {
			if ($xml->{'price-quotes'} ) {
				return $xml;
			}
			if ($xml->{'messages'} ) {					
				$messages = $xml->{'messages'}->children('http://www.canadapost.ca/ws/messages');		
				$error_from_curl='';
				foreach ( $messages as $message ) {
					$error_from_curl .= 'Error Code: ' . $message->code . "\n";
					$error_from_curl .= 'Error Msg: ' . $message->description . "\n\n";
				}
				if($this->email_errors) error_log("Error from cURL: " . $error_from_curl . " experienced by customer with id " . $_SESSION['customer_id'] . " on " . date('Y-m-d H:i:s'), 1, STORE_OWNER_EMAIL_ADDRESS);
			}
		}
		return false;
  }

 //  ***************************
  function exclude_choices($type) {
  	
	if(substr($type,0,4)=='DOM.')
		$type = 'D.'.substr($type,4);
	 else if(substr($km,0,4)=='USA.')
		$type = 'U.'.substr($type,4);
	 else if(substr($km,0,4)=='INT.')
		$type = 'I.'.substr($type,4);
	
    $disallowed_types = explode(",", MODULE_SHIPPING_CANADA_POST_TYPES);
    
    for ($za = 0; $za < count ($disallowed_types); $za++ ) {
      // when no disallowed types are present, --none-- is in the db but causes an error because --none-- is
      // not added as a define
      if ($disallowed_types[$za] == '--none--' ) continue; 
        if ($type == trim($disallowed_types[$za])) {
            return true;
        } // end if ($type_root == constant(trim($disallowed_types[$za]))).
    }
    // if the type is not disallowed:
    return false;
  }
// Next function used for sorting the shipping quotes on rate: low to high is default.
  function rate_sort_func ($a, $b) {
    
   $av = array_values($a);
   $av = $av[0];
   $bv = array_values($b);
   $bv = $bv[0];

//  return ($av == $bv) ? 0 : (($av < $bv) ? 1 : -1); // for having the high rates first
   return ($av == $bv) ? 0 : (($av > $bv) ? 1 : -1); // low rates first
  
  }
  
  function _GetRequestXml($index)
  {
	//http://www.canadapost.ca/cpo/mc/business/productsservices/developers/services/rating/getrates/default.jsf
	//The weight of the parcel in kilograms.
	//Details of the parcel dimensions in centimeters.

	$weight = $this->item_weight[$index];
	$price = round($this->item_price[$index],2);
	$width = $this->item_weight[$index];
	$height = $this->item_height[$index];
	$length = $this->item_length[$index];
	
	 if($this->unit_weight == 'LBS') {
	 	//change to kilograms
	 	$weight = round($weight/2.2,3);
     }
    if($this->unit_length == 'IN') {
		//change to centimeters 	  
		$width = round($width * 2.54,1);
		$height = round($height * 2.54,1);
		$length = round($length * 2.54,1);
	}
		  
	$xmlRequest ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>
	<mailing-scenario xmlns=\"http://www.canadapost.ca/ws/ship/rate\">
	  <customer-number>$this->access_customer_number</customer-number>
	  <quote-type>$this->quotetype</quote-type>";
	if($this->contract_id) {
	   $xmlRequest .= "<contract-id>$this->contract_id</contract-id>";	
	}  
	if (($this->use_insurance) && ($price>0)) {
		$xmlRequest .= "<options><option>
			  <option-code>COV</option-code>
			  <option-amount>$price</option-amount>
			</option>
		</options>";
	}
	$xmlRequest .= "  <parcel-characteristics>
		<weight>$weight</weight>";
	if (($this->dimensions_support>0) && ($width>0)	 && ($height>0)	 && ($length>0)) {
	   $xmlRequest .= "<dimensions><length>$length</length>
	   	<width>$width</width>
		<height>$height</height>
	   </dimensions>";
	}
	$xmlRequest .= "</parcel-characteristics>
	  <origin-postal-code>$this->_canadaPostOriginPostalCode</origin-postal-code>
	  <destination>";
	  if($this->_canadaPostDestCountryCode=='CA') {
		$xmlRequest .="<domestic>
		  <postal-code>$this->_canadaPostDestPostalCode</postal-code>
		</domestic>";
	  } else if($this->_canadaPostDestCountryCode=='US') {
		$xmlRequest .="<united-states>
		  <zip-code>$this->_canadaPostDestPostalCode</zip-code>
		</united-states>";
	  } else {
		$xmlRequest .="<international>
		  <country-code>$this->_canadaPostDestCountryCode</country-code>
		</international>";
	  }
	$xmlRequest .="  </destination>
	</mailing-scenario>";
	//echo $xmlRequest;
	return $xmlRequest;
  } //end of function _GetRequestXml
} //end of class canadapost
function get_multioption_canadapost_xml($values) 
{
	 if (tep_not_null($values)) {
		 $values_array = explode(',', $values);
		 foreach ($values_array as $key => $_method) {
		   if ($_method == '--none--') {
			 $method = $_method;
		   } else {
			 $method = constant('CANADA_POST_' .get_canada_post_service_code($_method));
		   }
		   $readable_values_array[] = $method;
		 }
		 $readable_values = implode(', ', $readable_values_array);
		 return $readable_values;
	 } else {
	   return '';
	 }
}
function get_canada_post_service_code($key)
{
	$km = str_replace('.','_',trim($key));
	 if(substr($km,0,2)=='D_')
		$km = 'DOM_'.substr($km,2);
	 else if(substr($km,0,2)=='U_')
		$km = 'USA_'.substr($km,2);
	 else if(substr($km,0,2)=='I_')
		$km = 'INT_'.substr($km,2);
	return $km;
}
  
function canadapost_cfg_select_multioption($select_array, $key_value, $key = '') 
{
	for ($i=0; $i<sizeof($select_array); $i++) {
	  $name = (($key) ? 'configuration[' . $key . '][]' : 'configuration_value');
	  $string .= '<br><input type="checkbox" name="' . $name . '" value="' . $select_array[$i] . '"';
	  $key_values = explode( ", ", $key_value);
	  if ( in_array($select_array[$i], $key_values) ) $string .= ' CHECKED';
	  $string .= '> ' . constant('CANADA_POST_' . get_canada_post_service_code($select_array[$i]));
	} 
	$string .= '<input type="hidden" name="' . $name . '" value="--none--">';
	return $string;
}	
?>
