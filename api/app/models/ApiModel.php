<?php

namespace models;

use DB\SQL;
use DB\SQL\Mapper;
use DB\SQL\Schema;


/**
 * Class ApiModel
 * @package models
 */
class ApiModel
{

    /**
     * @var \DB\SQL
     */
    private $db;
    /**
     * @var ApiSQLModel
     */
    public $mapper;

    /**
     *
     */
    function __construct()
    {

        $f3 = \Base::instance();

        $this->db = new SQL(
            "mysql:host=" . $f3->get('MYSQL_HOST') .
            ";port=" . $f3->get('MYSQL_PORT') .
            ";dbname=" . $f3->get('MYSQL_DB'),
            $f3->get('MYSQL_USER'),
            $f3->get('MYSQL_PASS'));

        $this->mapper = new ApiSQLModel($this->db, 'oas_api');

        // hooks
        $this->mapper->oninsert('\models\LogsModel->create_log_table');
        $this->mapper->onerase('\models\LogsModel->delete_log_table');


    }

    function update()
    {

        $schema = new Schema($this->db);
        $table = $schema->alterTable('oas_api');

        // is_public field
        $cols = $table->getCols();
        if (!in_array('is_public', $cols))
            $table->addColumn('is_public')->type($schema::DT_BOOLEAN)->defaults(0);

        $table->build();

    }

    /**
     * @param $api
     * @return array
     */
    function validate($api)
    {

        if (!$api['name'])
            return array(
                'status' => 'error',
                'message' => 'The name is incorrect'
            );

        if (strpos($api['path'], '/') === false)
            return array(
                'status' => 'error',
                'message' => 'The path is incorrect'
            );

        if ($this->mapper->count(array('path=? and id!= ?', $api['path'], $api['id'])))
            return array(
                'status' => 'error',
                'message' => 'The path is duplicated'
            );

        return array(
            'status' => 'success',
        );

    }

    /**
     * @return array
     */
    function seed()
    {

        return array(
            'name' => '',
            'description' => '',
            'path' => '',
            'cache' => 0,
            'throttling' => 0,
            'method' => 'all',
            'output' => 'json',
            'is_public' => 0,
            'action' => array(
                'type' => 'printer',
                'params' => array(),
                'action' => '',
                'after' => 'return $result;'
            ),
            'security' => array(
                'type' => 'none',
                'params' => array(),
                'encryptation' => 'plain',
                'error' => 'no have permissions for this REQUEST'
            ),
        );

    }


}

/**
 * Class ApiSQLModel
 * @package models
 */
class ApiSQLModel extends Mapper
{

    /**
     * @return object
     */
    function object()
    {

        $result = $this->cast();

        if ($result['action'])
            $result['action'] = unserialize($result['action']);
        if ($result['security'])
            $result['security'] = unserialize($result['security']);

        return (object)$result;

    }

//    function save()
//    {
//        return False;
//    }
//
//    function erase($filter = NULL)
//    {
//        return False;
//    }


}
