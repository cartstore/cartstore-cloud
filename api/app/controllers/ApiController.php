<?php

namespace controllers;

/**
 * Class ApiController
 * @package controllers
 */
class ApiController
{

    /**
     * @param \Base $f3
     * @param $params
     */
    function get(\Base $f3, $params)
    {
        // get GET params
        $get = $f3->get('GET');

        // scrub params
        $f3->scrub($get);
        $f3->scrub($params);

        // remove param 0 = url
        unset($params[0]);

        // join params
        $params = array_merge($params, $get);

        // exec the API
        if ($f3->get('api')->method == 'get' or $f3->get('api')->method == 'all')
            $this->execute($params);
        else
            // trigger error 404
            $f3->error(404);
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    function post(\Base $f3, $params)
    {
        // get POST params
        $post = $f3->get('POST');

        // scrub params
        $f3->scrub($post);
        $f3->scrub($params);

        // remove param 0 = url
        unset($params[0]);

        // join params
        $params = array_merge($params, $post);

        // exec the API
        if ($f3->get('api')->method == 'post' or $f3->get('api')->method == 'all')
            $this->execute($params);
        else
            // trigger error 404
            $f3->error(404);
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    function put(\Base $f3, $params)
    {
        // get PUT params
        $put = $f3->get('PUT');

        // scrub params
        $f3->scrub($put);
        $f3->scrub($params);

        // remove param 0 = url
        unset($params[0]);

        // join params
        $params = array_merge($params, $put);

        // exec the API
        if ($f3->get('api')->method == 'put' or $f3->get('api')->method == 'all')
            $this->execute($params);
        else
            // trigger error 404
            $f3->error(404);
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    function delete(\Base $f3, $params)
    {
        // get DELETE params
        $delete = $f3->get('DELETE');

        // scrub params
        $f3->scrub($delete);
        $f3->scrub($params);

        // remove param 0 = url
        unset($params[0]);

        // join params
        $params = array_merge($params, $delete);

        // exec the API
        if ($f3->get('api')->method == 'delete' or $f3->get('api')->method == 'all')
            $this->execute($params);
        else
            // trigger error 404
            $f3->error(404);
    }

    /**
     * @param $params
     */
    private function execute($params)
    {

        $f3 = \Base::instance();

        // check the security
        if ($f3->get('s_security')->check($params)) {
            // execute the action
            $result = $f3->get('s_action')->execute($params);
            // execute the s_middleware
            $result = $f3->get('s_middleware')->after($result);
            // send the result
            $f3->get('s_output')->send($result, $f3->get('api')->output);
            // send to logs
            $f3->get('s_logger')->write($params);
        } else
            if (isset($f3->get('api')->security['error']))
                $f3->get('s_output')->send(array('error' => $f3->get('api')->security['error']), $f3->get('api')->output);
            else
                $f3->get('s_output')->send(array('error' => 'no have permissions for this REQUEST'), $f3->get('api')->output);
    }

    /**
     * @param $f3
     */
    function beforeRoute(\Base $f3)
    {
        $f3->set('api', $f3->get('s_api')->mapper->load(array('path = ?', $f3->get('PATTERN')))->object());
    }

} 