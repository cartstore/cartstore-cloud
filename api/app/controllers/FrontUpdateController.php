<?php

namespace controllers;

require_once '3rdparty/Config/Lite.php';

use models\ApiModel;

class FrontUpdateController
{
    /**
     * @param \Base $f3
     * @param $params
     */
    function update(\Base $f3, $params)
    {

        $api = new ApiModel();
        $api->update();

        \Util::rrmdir('data/cache');
        \Util::rrmdir('data/temp');
        \Util::rrmdir('data/uploads');

        $config = new \Config_Lite('data/config.ini', LOCK_EX);
        $config->set('globals', 'WWW_DOC', "http://rabartu.info/documentation/ourapiserver")
            ->set('globals', 'WWW_NEWS', "http://rabartu.info/category/products/ourapiserver");
        $config->save();

        $f3->set('content', 'page_update.html');
        echo \Template::instance()->render('__layout.html');


    }

}