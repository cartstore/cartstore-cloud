<?php

namespace controllers;

/**
 * Class FrontEditController
 * @package controllers
 */
class FrontEditController
{

    /**
     * Add new Api
     * @param \Base $f3
     */
    function add(\Base $f3)
    {
        $f3->set('id', 'new');

        $f3->set('l_output', $f3->get('s_output')->list_all());
        $f3->set('l_action', $f3->get('s_action')->list_all());
        $f3->set('l_security', $f3->get('s_security')->list_all());
        $f3->set('l_encryption', $f3->get('s_encryption')->list_all());

        $f3->set('content', 'page_edit.html');
        echo \Template::instance()->render('__layout.html');
    }

    /**
     * Clone Api
     * @param \Base $f3
     * @param $params
     */
    function dolly(\Base $f3, $params)
    {
        $f3->set('id', 'new');
        $f3->set('clone', $params['id']);

        $f3->set('l_output', $f3->get('s_output')->list_all());
        $f3->set('l_action', $f3->get('s_action')->list_all());
        $f3->set('l_security', $f3->get('s_security')->list_all());
        $f3->set('l_encryption', $f3->get('s_encryption')->list_all());

        $f3->set('content', 'page_edit.html');
        echo \Template::instance()->render('__layout.html');
    }

    /**
     * Save Api
     * @param \Base $f3
     * @param $params
     */
    function save(\Base $f3, $params)
    {

        $post = $f3->get('POST');
        // $f3->scrub($post);

        $api_mapper = $params['id'] != 'new' ?
            $f3->get('s_api')->mapper->load(array('id=?', $params['id'])) : $f3->get('s_api')->mapper;

        // i basic
        $api_mapper->name = $post['name'];
        $api_mapper->description = $post['description'];
        $api_mapper->path = $post['path'];
        $api_mapper->cache = $post['cache'];
        $api_mapper->throttling = $post['throttling'];
        $api_mapper->method = $post['method'];
        $api_mapper->output = $post['output'];
        $api_mapper->is_public = isset($post['is_public']) ? $post['is_public'] : 0;
        // ii source
        $api_mapper->action = serialize(array(
            'type' => $post['source'],
            'params' => isset($post['action_param']) ? $post['action_param'] : array(),
            'action' => $post['action'],
            'after' => $post['after']
        ));
        // iii security
        $api_mapper->security = serialize(array(
            'type' => $post['security'],
            'params' => isset($post['security_param']) ? $post['security_param'] : array(),
            'encryption' => $post['encryption'],
            'error' => $post['security_error']
        ));
        // extra
        $api_mapper->last_change = date("Y-m-d H:i:s");


        $api_mapper->save();

        $f3->reroute('/front/edit/' . $api_mapper->id);

    }

    /**
     * Edit Api
     * @param \Base $f3
     * @param $params
     */
    function edit(\Base $f3, $params)
    {

        $f3->set('id', $params['id']);

        $f3->set('l_output', $f3->get('s_output')->list_all());
        $f3->set('l_action', $f3->get('s_action')->list_all());
        $f3->set('l_security', $f3->get('s_security')->list_all());
        $f3->set('l_encryption', $f3->get('s_encryption')->list_all());

        $f3->set('content', 'page_edit.html');
        echo \Template::instance()->render('__layout.html');

    }

    /**
     * Remove Api
     * @param \Base $f3
     * @param $params
     */
    function remove(\Base $f3, $params)
    {

        $f3->get('s_api')->mapper->load(array('id=?', $params['id']));
        $f3->get('s_api')->mapper->erase();

        $f3->reroute('/front');

    }

    /**
     * Get the Api via AJAX
     * @param \Base $f3
     * @param $params
     */
    function ajax_get_api(\Base $f3, $params)
    {
        if ($params['id'] != 'new') {
            $mapper = $f3->get('s_api')->mapper->load(array('id=?', $params['id']));
            echo json_encode(
                $mapper->object()
            );
        } else {
            echo json_encode(
                $f3->get('s_api')->seed()
            );
        }
    }

    /**
     * Validate Api's values via AJAX
     * @param \Base $f3
     * @param $params
     */
    function ajax_validate_api(\Base $f3, $params)
    {

        $post = $f3->get('POST');
        $f3->scrub($post);

        echo json_encode($f3->get('s_api')->validate($post));

    }

    /**
     * Get source params
     * @param \Base $f3
     * @param $params
     */
    function ajax_get_source(\Base $f3, $params)
    {

        $action = $params['action'];

        echo json_encode(array(
            'mode' => $f3->get('s_action')->mode($action),
            'fields' => $f3->get('s_action')->fields($action),
            'help' => $f3->get('s_action')->help($action)
        ));

    }

    /**
     * Get security params
     * @param \Base $f3
     * @param $params
     */
    function ajax_get_security(\Base $f3, $params)
    {

        $action = $params['security'];

        echo json_encode(array(
            'fields' => $f3->get('s_security')->fields($action),
            'help' => $f3->get('s_security')->help($action)
        ));

    }

    /**
     * @param $f3
     */
    function beforeRoute(\Base $f3)
    {
        if (!$f3->get('SESSION.is_logged'))
            $f3->reroute('/front/login?error=no have permissions.');

        $f3->set('section_active', 'edit');
    }

} 