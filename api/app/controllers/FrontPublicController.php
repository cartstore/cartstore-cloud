<?php

namespace controllers;


class FrontPublicController
{
    /**
     * @param \Base $f3
     * @param $params
     */
    function index(\Base $f3, $params)
    {
        $l_api = $f3->get('s_api')->mapper->find();
        $f3->set('l_api', $l_api);

        echo \Template::instance()->render('page_public.html');

    }

} 