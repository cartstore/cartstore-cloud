<?php

namespace controllers;


/**
 * Class FrontSecurityController
 * @package controllers
 */
class FrontSecurityController
{

    /**
     * @param \Base $f3
     */
    function login(\Base $f3)
    {

        if ($f3->get('SESSION.is_logged'))
            $f3->reroute('/front');

        $f3->set('content', 'page_login.html');
        echo \Template::instance()->render('__layout.html');
    }

    /**
     * @param \Base $f3
     */
    function check_login(\Base $f3)
    {

        $post = $f3->get('POST');
        $f3->scrub($post);

        if ($post['username'] == $f3->get('ADMIN_USER') and $post['password'] == $f3->get('ADMIN_PASS')) {
            $f3->set('SESSION.is_logged', TRUE);
            $f3->reroute('/front');
        } else
            $f3->reroute('/front/login?error=Bad Credentials.');
    }

    /**
     * @param \Base $f3
     */
    function logout(\Base $f3)
    {
        $f3->clear('SESSION');
        $f3->reroute('/front/login');
    }

}
