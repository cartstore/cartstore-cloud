DROP TABLE IF EXISTS `oas_api`;
CREATE TABLE `oas_api` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `path` varchar(50) NOT NULL,
  `cache` int(11) NOT NULL,
  `throttling` int(11) NOT NULL,
  `method` varchar(50) NOT NULL,
  `output` varchar(50) NOT NULL,
  `action` text NOT NULL,
  `security` text NOT NULL,
  `last_change` datetime NOT NULL,
  `is_public` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


DROP TABLE IF EXISTS `oas_test`;
CREATE TABLE `oas_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;