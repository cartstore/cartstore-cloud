<?php

namespace plugins;


/**
 * Interface Output
 * @package plugins
 */
interface Output
{
    /**
     * @param $data
     * @return mixed
     */
    public static function send($data);

} 