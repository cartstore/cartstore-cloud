<?php

namespace plugins;


/**
 * Interface Action
 * @package plugins
 */
interface Action
{

    /**
     * @param $params
     * @return mixed
     */
    public static function run($params);

    /**
     * @return mixed
     */
    public static function fields();

    /**
     * @return mixed
     */
    public static function help();

} 