<?php

namespace plugins;


/**
 * Interface Security
 * @package plugins
 */
interface Security
{

    /**
     * @param $params
     * @return mixed
     */
    public static function check($params);

    /**
     * @return mixed
     */
    public static function fields();

    /**
     * @return mixed
     */
    public static function help();

} 