<?php


class Security
{

    public static function check($params)
    {
        $f3 = \Base::instance();

        if (!$security = $f3->get('api')->security['type'])
            return true;

        if (Security::exist($security))
            return call_user_func_array('\\security\\' . $security . '::check', array($params));

    }

    public static function exist($security)
    {
        return file_exists('plugins/security/' . $security . '.php');
    }

    public static function list_all()
    {
        $result = array();
        $sec = array_diff(scandir('plugins/security/'), array('..', '.'));
        foreach ($sec as $l)
            $result[] = array(
                'name' => basename($l, ".php"),
                'fields' => Security::fields($l),
                'help' => Security::help($l),
            );

        return $result;
    }

    public static function fields($l)
    {

        return call_user_func_array('\\security\\' . basename($l, ".php") . '::fields', array());
    }

    public static function help($l)
    {

        return call_user_func_array('\\security\\' . basename($l, ".php") . '::help', array());
    }

}