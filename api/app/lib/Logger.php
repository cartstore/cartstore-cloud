<?php

use models\LogsModel;

class Logger
{

    public static function write($params)
    {

        $f3 = \Base::instance();

        $log = new LogsModel();

        $log->connect($f3->get('api')->id);

        $log->mapper->host = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $log->mapper->url = $_SERVER['REQUEST_URI'];
        $log->mapper->params = serialize($params);
        $log->mapper->agent = $_SERVER['HTTP_USER_AGENT'];
        $log->mapper->datetime = date("Y-m-d H:i:s");

        $log->mapper->insert();

    }

} 