<?php


class Output
{
    public static function send($data, $format)
    {
        if (Output::exist($format))
            call_user_func_array('\\output\\' . $format . '::send', array($data));
    }

    public static function exist($format)
    {
        return file_exists('plugins/output/' . $format . '.php');
    }

    public static function list_all()
    {
        $result = array();

        $list = array_diff(scandir('plugins/output/'), array('..', '.'));
        foreach ($list as $l)
            $result[] = array(
                'name' => basename($l, ".php")
            );

        return $result;
    }

} 