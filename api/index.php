<?php

// wfq !!!
header('Access-Control-Allow-Origin: *');

// check if is installed
if (!file_exists('data/config.ini')) {
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'install.php';
    $protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
    header("Location: $protocol://$host$uri/$extra");
    exit;
}

// framework core
$f3 = require('lib/base.php');

// version
$f3->set('V', '2.1.0');

// config
$f3->config('app/routes.ini');
$f3->config('data/config.ini');

// services
$f3->set('s_api', new models\ApiModel());

// services
$f3->set('s_output', new Output());
$f3->set('s_security', new Security());
$f3->set('s_encryption', new Encryption());
$f3->set('s_action', new Action());
$f3->set('s_middleware', new Middleware());
$f3->set('s_logger', new Logger());

// route mapper
foreach ($f3->get('s_api')->mapper->find() as $api)
    $f3->map($api['path'], '\controllers\ApiController', $api['cache'], $api['throttling']);

// error handler
$f3->set('ONERROR',
    function ($f3) {
        if (strlen($f3->get('AGENT')) > 20) { // guayaba !!!
            $f3->set('content', 'page_error.html');
            echo \Template::instance()->render('__layout.html');
        } else {
            echo "ERROR " . $f3->get('ERROR.code') . " : \"" . $f3->get('ERROR.text') . "\"";
        }
    }
);

// app
$f3->run();