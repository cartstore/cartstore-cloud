$(function () {

    // hide|show advanced options
    $('.action-after').fadeOut();

    $("#show_advanced_opt").click(function () {
        $('.action-after').fadeToggle('fast', function () {
            var link = $("#show_advanced_opt");
            link.html((link.text() == '{show advanced options}') ? '{hide advanced actions}' : '{show advanced options}');
        });
    }).html('{show advanced options}');

    codemirror_mode($('textarea[name=after]'), 'php');

    // fill 'source' params when change 'source' select
    $("select[name=source]").change(function () {
        in_loading_state();
        fill_source_fields(this.value);
        out_loading_state();
    });

    // fill 'security' params when change 'security' select
    $("select[name=security]").change(function () {
        fill_security_fields(this.value);
    });

    // checkboxs field
    $("input[type=checkbox]").change(function () {
        if ($(this).is(':checked')) {
            $(this).val(1);
        } else {
            $(this).val(0);
        }
    });

});

/**
 * Validate and Save API.
 */
function save_api() {

    $.ajax({
        url: "../ajax_validate_api",
        type: 'post',
        dataType: 'json',
        async: false,
        data: $('#form-edit').serialize(),
        success: function (data) {

            if (data.status == 'success') {
                $('#form-edit').submit();
            } else {

                // error
                var s = '<div class="alert alert-dismissable alert-block alert-danger" style="margin-bottom: 5px;">';
                s += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                s += '<p style="margin-left:10px;"><strong>{Opss}</strong> ' + data.message + '</p>';
                s += '</div>';

                $('.validation-errors').html(s);

            }


        }
    });
}

/**
 * Create Security fields Inputs
 * @param security
 */
function fill_security_fields(security) {

    $.ajax({

        url: "../ajax_get_security/" + security,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function (data) {

            // help
            $('.security-help').html('<i class="fa fa-lightbulb-o"></i> ' + data.help);

            // fields
            var s = '';
            $.each(data.fields, function (index, value) {
                s += '<div class="form-group">';
                s += '<label for="input_security_"' + index + ' class="col-sm-2 control-label">' + value.title + '</label>';
                s += '<div class="col-sm-10">';
                if (value.type == 'textarea') {
                    s += '<textarea class="form-control" id="input_security_' + index + '" name="security_param[' + index + ']" placeholder="' + value.title + '"></textarea>';
                } else {
                    s += '<input type="' + value.type + '" class="form-control" id="input_security_' + index + '" name="security_param[' + index + ']" placeholder="' + value.title + '">';
                }
                s += '</div>';
                s += '<div class="col-sm-offset-2 col-sm-10">';
                s += '<small class="help-block">' + value.help + '</small>';
                s += '</div>';
                s += '</div>';
                s += '</div>';
            });

            // header
            if (s) {
                h = '<h4 class="page-header">Params';
                h += '<small> in this section we define the params for the API\'s security.</small>';
                h += '</h4>';
                s = h + s;
            }

            $('.security-params').html(s);


            // encryption field
            $('select[name=encryption]').val('plain');

        }
    });

    // hide fields
    if (security == 'none') {
        $('select[name=encryption]').parent().parent().hide();
        $('input[name=security_error]').parent().parent().hide();
    } else {
        $('select[name=encryption]').parent().parent().show();
        $('input[name=security_error]').parent().parent().show();
    }

}

/**
 * Create Source fields Inputs
 * @param action
 */
function fill_source_fields(action) {

    $.ajax({

        url: "../ajax_get_source/" + action,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function (data) {

            // help
            $('.source-help').html('<i class="fa fa-lightbulb-o"></i> ' + data.help);

            // fields
            var s = '';
            $.each(data.fields, function (index, value) {
                s += '<div class="form-group">';
                s += '<label for="input_action_"' + index + ' class="col-sm-2 control-label">' + value.title + '</label>';
                s += '<div class="col-sm-10">';
                s += '<input type="' + value.type + '" class="form-control" id="input_action_' + index + '" name="action_param[' + index + ']" placeholder="' + value.title + '">';
                s += '</div>';
                s += '<div class="col-sm-offset-2 col-sm-10">';
                s += '<small class="help-block">' + value.help + '</small>';
                s += '</div>';
                s += '</div>';
                s += '</div>';
            });

            // header
            if (s) {
                h = '<h4 class="page-header">Params';
                h += '<small> in this section we define the params for the API\'s action.</small>';
                h += '</h4>';
                s = h + s;
            }

            $('.action-params').html(s);

            codemirror_mode($('textarea[name=action]'), data.mode);

        }
    });
}

/**
 * Fill all Inputs with the API's values.
 * @param id
 */
function fill_api_fields(id) {

    in_loading_state();

    // CKEDITOR to make easy the description
    CKEDITOR.replace('input_description');

    $.ajax({

        url: "../ajax_get_api/" + id,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function (data) {

            // id
            $('input[name=id]').val(data.id);

            // i basic
            $('input[name=name]').val(data.name);
            $('textarea[name=description]').val(data.description);
            $('input[name=path]').val(data.path);
            $('input[name=cache]').val(data.cache);
            $('input[name=throttling]').val(data.throttling);
            $('select[name=method]').val(data.method);
            $('select[name=output]').val(data.output);
            $('input[name=is_public]').val(data.is_public).prop('checked', data.is_public);

            // ii action
            $('select[name=source]').val(data.action.type);
            fill_source_fields(data.action.type);
            $.each(data.action.params, function (index, value) {
                $('input[name="action_param[' + index + ']"]').val(value);
            });
            action_selector = $('textarea[name=action]');
            action_selector.val(data.action.action);
            codemirror_value(action_selector, data.action.action);
            after_selector = $('textarea[name=after]');
            after_selector.val(data.action.after ? data.action.after : 'return $result;');
            codemirror_value(after_selector, data.action.after);

            // iii security
            $('select[name=security]').val(data.security.type);
            fill_security_fields(data.security.type);
            $.each(data.security.params, function (index, value) {
                $('#input_security_' + index).val(value);
            });
            $('select[name=encryption]').val(data.security.encryption);
            // legacy compatibility
            if (!data.security.error) {
                $('input[name=security_error]').val('no have permissions for this REQUEST');
            } else {
                $('input[name=security_error]').val(data.security.error);
            }


        }
    });

    out_loading_state();
}

/**
 * Set Value of the CodeMirror Instance
 * @param selector
 * @param value
 */
function codemirror_value(selector, value) {

    if (selector.data('codemirror'))
        selector.data('codemirror').setValue(value);

}

/**
 * Convert Inputs to CodeMirror Instance
 * @param selector
 * @param mode
 */
function codemirror_mode(selector, mode) {

    if (selector.data('codemirror')) {

        editor = selector.data('codemirror');
        editor.setOption('mode', 'text/x-' + mode);

    } else {

        editor = CodeMirror.fromTextArea(selector[0], {
            lineNumbers: true,
            matchBrackets: true,
            mode: 'text/x-' + mode,
            indentUnit: 4,
            indentWithTabs: true,
            enterMode: "keep",
            tabMode: "shift",
            viewportMargin: 4
        });
        editor.on("change", function () {
            selector.val(editor.getValue());
        });
        selector.data('codemirror', editor);

    }


}


