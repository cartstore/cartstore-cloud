function fill_api_stats(id) {

    in_loading_state();

    $.ajax({

        url: "../ajax_group_by_hits/" + id,
        type: 'get',
        dataType: 'json',
        async: false,
        data: $('#form-filter').serialize(),
        success: function (series) {
            $.plot('#stats', series, {
                yaxis: {
                    min: 0
                },
                xaxis: {
                    mode: "time"
                }
            });
        }

    });


    out_loading_state();

}
