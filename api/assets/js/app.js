function in_loading_state() {
    $('.container').css('opacity', 0.2);
    $('#loading').css('display', 'block');
}

function out_loading_state() {
    $('.container').css('opacity', 1);
    $('#loading').css('display', 'none');
}