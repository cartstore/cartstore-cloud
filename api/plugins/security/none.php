<?php

namespace security;

use plugins\Security;

class none implements Security
{
    public static function check($params)
    {
        return TRUE;
    }

    public static function fields()
    {
        return array();
    }

    public static function help()
    {
        return "No security :-(";
    }

}
