<?php

namespace action;

use plugins\Action;

class ldap implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        $url = $f3->get('api')->action['params']['url'];
        $port = $f3->get('api')->action['params']['port'] ? $f3->get('api')->action['params']['port'] : 389;
        $base = $f3->get('api')->action['params']['base'];
        $attributes = explode(';', $f3->get('api')->action['params']['attributes']);
        $user = $f3->get('api')->action['params']['user'];
        $pass = $f3->get('api')->action['params']['pass'];
        $query = $f3->get('api')->action['action'];

        $query = \Util::translate($query, $params);

        $ldap = @ldap_connect($url, $port);

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        if ($user)
            ldap_bind($ldap, $user, $pass);

        $result = ldap_search($ldap, $base, $query, $attributes);

        return ldap_get_entries($ldap, $result);
    }

    public static function fields()
    {
        return array(
            'url' => array('type' => 'text', 'title' => 'URL', 'help' => 'ldap url'),
            'port' => array('type' => 'text', 'title' => 'Port', 'help' => 'ldap port'),
            'base' => array('type' => 'text', 'title' => 'Base DN', 'help' => 'ldap base example: <code>DC=gmail,DC=com</code>'),
            'attributes' => array('type' => 'text', 'title' => 'Attributes', 'help' => 'ldap attributes'),
            'user' => array('type' => 'text', 'title' => 'User', 'help' => 'ldap user example: <code>user@gmail.com</code>'),
            'pass' => array('type' => 'password', 'title' => 'Password', 'help' => 'ldap password'),
        );
    }

    public static function help()
    {
        return '
                In the Action Field put a LDAP Search Filter.
                Can use the <b>Parameters</b> with the prefix <code>@</code> example: <code>@user</code>.<br>
                ';
    }

}
