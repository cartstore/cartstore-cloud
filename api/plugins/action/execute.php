<?php

namespace action;

use plugins\Action;

class execute implements Action
{

    public static function run($params)
    {
        $f3 = \Base::instance();

        $action = $f3->get('api')->action['action'];
        $action = \Util::translate($action, $params);

        return array(shell_exec($action));

    }

    public static function fields()
    {
        return array();
    }

    public static function help()
    {
        return '
                In the Action Field put a System Sentence for Execute on the Operative System <em>(OS)</em><br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                ';
    }

}
