<?php

namespace action;

use plugins\Action;

class mysql implements Action
{
    public static function run($params)
    {
        $f3 = \Base::instance();

        $db_host = $f3->get('api')->action['params']['db_host'];
        $db_port = $f3->get('api')->action['params']['db_port'] ? $f3->get('api')->action['params']['db_port'] : 3306;
        $db_name = $f3->get('api')->action['params']['db_name'];
        $db_user = $f3->get('api')->action['params']['db_user'];
        $db_pass = $f3->get('api')->action['params']['db_pass'];
        $db_query = $f3->get('api')->action['action'];

        $db_query = \Util::translate($db_query, $params);

        $db = new \DB\SQL('mysql:host=' . $db_host . ';port=' . $db_port . ';dbname=' . $db_name, $db_user, $db_pass);

        return $db->exec($db_query);

    }

    public static function mode()
    {
        return 'mysql';
    }

    public static function fields()
    {
        return array(
            'db_host' => array('type' => 'text', 'title' => 'Database Host', 'help' => 'MySQL Server Host'),
            'db_port' => array('type' => 'text', 'title' => 'Database Port', 'help' => 'MySQL Server Port (leave in blank to use "3306")'),
            'db_name' => array('type' => 'text', 'title' => 'Database Name', 'help' => 'DataBase Name'),
            'db_user' => array('type' => 'text', 'title' => 'Database User', 'help' => 'DataBase User'),
            'db_pass' => array('type' => 'password', 'title' => 'Database Password', 'help' => 'DataBase Password')
        );
    }

    public static function help()
    {
        return '
                In the Action Field put a SQL Query.<br>
                Can use the <b>Parameters</b> with the prefix <code>@</code> example <code>@option</code>.<br>
                ';
    }

}
