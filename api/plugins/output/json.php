<?php


namespace output;


use plugins\Output;

class json implements Output
{
    public static function send($data)
    {
        $output = new \Luracast\Restler\Format\JsonFormat;
        echo $output->encode($data, true);
    }
} 