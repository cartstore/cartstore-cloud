<?php

namespace output;

use plugins\Output;

class serialize implements Output
{
    public static function send($data)
    {
        echo serialize($data);
    }

}
