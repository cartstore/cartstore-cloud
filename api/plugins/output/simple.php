<?php

namespace output;

use plugins\Output;

class simple implements Output
{
    public static function send($data)
    {
        echo implode($data);
    }

}
