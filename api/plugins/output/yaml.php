<?php

namespace output;

use plugins\Output;

class yaml implements Output
{
    public static function send($data)
    {
        $output = new \Luracast\Restler\Format\YamlFormat;
        echo $output->encode($data, true);
    }

}
