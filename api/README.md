OurApiServer v2
===============

OurAPIServer is a tool for create a Restful service interface for our projects.
The main objective of the project is the creation of a standard tool with supports for many existing projects,
including Wordpress, Drupal, etc. Also does not limit to MySQL data source,
it can be widely extended easily with the use of plugins.
