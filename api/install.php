<?php

// check if is installed
if (file_exists('data/config.ini')) {
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'front';
    $protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
    header("Location: $protocol://$host$uri/$extra");
    exit;
}

$section = null;
if (isset($_GET['section']))
    $section = $_GET['section'];
if (isset($_POST['section']))
    $section = $_POST['section'];
if (!$section)
    $section = 'enviroment';

if ($section == 'enviroment') {

    $enviroment = array();
    $enviroment_ok = true;

    // PHP version
    $v = explode('.', phpversion());
    $status = ($v[0] >= 5 and $v[1] >= 3) ? true : false;
    $enviroment[] = array(
        'variable' => 'PHP Version',
        'current' => phpversion(),
        'required' => '+5.3',
        'status' => $status ? 'OK' : 'ERROR'
    );
    if (!$status)
        $enviroment_ok = false;

    // Module Rewrite
    // $status = in_array('mod_rewrite', apache_get_modules());
    $enviroment[] = array(
        'variable' => 'Apache2 Module Rewrite',
        'current' => 'Unknow',
        'required' => 'Installed',
        'status' => 'Unknow',
    );
    // if (!$status)
    //    $enviroment_ok = false;

    // mysqli Extension
    $status = extension_loaded('mysqli');
    $enviroment[] = array(
        'variable' => 'MySQL Improved (mysqli) Extension',
        'current' => $status ? 'Installed' : 'Unknow',
        'required' => 'Installed',
        'status' => $status ? 'OK' : 'ERROR',
    );
    if (!$status)
        $enviroment_ok = false;

    // PDO Extension
    $status = extension_loaded('pdo');
    $enviroment[] = array(
        'variable' => 'PHP Data Objects (pdo) Extension',
        'current' => $status ? 'Installed' : 'Unknow',
        'required' => 'Installed',
        'status' => $status ? 'OK' : 'ERROR',
    );
    if (!$status)
        $enviroment_ok = false;

}

if ($section == 'save_configuration') {

    $error_configuration = false;

    // Database Validation
    if (!$link = @mysqli_connect($_POST['db_host'], $_POST['db_user'], $_POST['db_password'], $_POST['db_name'], $_POST['db_port']))
        $error_configuration = 'Bad Database Configuration';

    // URL Validation
    if (!$error_configuration and !filter_var($_POST['url'], FILTER_VALIDATE_URL))
        $error_configuration = 'Bad URL Configuration';

    // FileSystem Permissions
    if (!$error_configuration and !file_exists("data/"))
        if (!$error_configuration and !mkdir("data/"))
            $error_configuration = 'No have permissions for create the <code>data</code> in the root of the project.';
        else
            if (!$error_configuration and !@copy('app/config.ini.dist', 'data/config.ini'))
                $error_configuration = 'No have permissions for the Configuration';
            else
                file_put_contents("data/index.html", "");

    // Security Validation
    if (!$error_configuration and (!$_POST['user'] or !$_POST['password']))
        $error_configuration = 'Bad Security User';

    if (!$error_configuration) {
        // Create DB SCHEMA and config
        $schema = file_get_contents('app/schema.sql');
        mysqli_multi_query($link, $schema);
        // Create config File
        require_once '3rdparty/Config/Lite.php';
        $config = new \Config_Lite('data/config.ini', LOCK_EX);
        $config->set('globals', 'WWW', $_POST['url'])
            ->set('globals', 'MYSQL_HOST', $_POST['db_host'])
            ->set('globals', 'MYSQL_PORT', $_POST['db_port'])
            ->set('globals', 'MYSQL_DB', $_POST['db_name'])
            ->set('globals', 'MYSQL_USER', $_POST['db_user'])
            ->set('globals', 'MYSQL_PASS', $_POST['db_password'])
            ->set('globals', 'ADMIN_USER', $_POST['user'])
            ->set('globals', 'ADMIN_PASS', $_POST['password']);
        $config->save();
        $section = 'finish';
    } else
        $section = 'configuration';

}

if ($section == 'configuration') {
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
    $url = "$protocol://$host$uri";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>OurApiServer - the fast way to create APIs!</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" media="screen">
    <link rel="stylesheet" href="assets/css/styles.css" media="screen">

    <link rel="shortcut icon" href="assets/img/fav.png">

    <script src="assets/js/jquery.min.js"></script>

</head>
<body>

<div class="container">

<div class="row p-header">
    <div class="col-sm-10">
        <h1><a href="javascript:void(0)"><i class="fa fa-cogs"></i></a> OurApiServer
            <small>{the fast way to create APIs}</small>
        </h1>
    </div>
</div>

<br>

<div class="row">

<div class="col-sm-offset-1  col-sm-2">

    <div class="right-section">

        <ul class="nav nav-pills nav-stacked">

            <li class="<?php if ($section == 'enviroment') echo 'active' ?>">
                <a href="javascript:void(0)" title="Enviroment">I. Enviroment</a>
            </li>
            <li class="<?php if ($section == 'configuration') echo 'active' ?>">
                <a href="javascript:void(0)" title="Configuration">II. Configuration</a>
            </li>
            <li class="<?php if ($section == 'finish') echo 'active' ?>">
                <a href="javascript:void(0)" title="Finish">III. Finish</a>
            </li>

        </ul>

    </div>

</div>

<div class="col-sm-8">

    <div class="left-section">

        <h3>
            <i class="fa fa-signal"></i> Installation
        </h3>

        <?php if ($section == 'enviroment') { ?>

            <h3 class="page-header">Enviroment
                <small>{checking enviroment variables}</small>
            </h3>
            <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Current</th>
                        <th>Required</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($enviroment as $i) { ?>
                        <tr>
                            <td><?php echo $i['variable']; ?></td>
                            <td><?php echo $i['current']; ?></td>
                            <td><?php echo $i['required']; ?></td>
                            <td><?php echo $i['status']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <hr>
            <?php if ($enviroment_ok) { ?>
                <a href="install.php?section=configuration" class="btn btn-default">
                    Next
                </a>
            <?php } ?>

        <?php } ?>

        <?php if ($section == 'configuration') { ?>
            <form class="form-horizontal" action="install.php"
                  method="POST">

                <input type="hidden" name="section" value="save_configuration"/>

                <?php if (isset($error_configuration)) { ?>

                    <div class="alert alert-dismissable alert-block alert-danger"
                         style="margin-bottom: 5px;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <p style="margin-left:10px;"><strong>{Opss}</strong> <?php echo $error_configuration; ?>
                        </p>
                    </div>

                <?php } ?>

                <h3 class="page-header">Site
                    <small>{}</small>
                </h3>

                <div class="form-group">
                    <label for="input_url" class="col-sm-2 control-label">URL</label>

                    <div class="col-sm-6">
                        <input class="form-control" id="input_url" placeholder="URL" name="url"
                               value="<?php echo $url; ?>"/>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">URL of the project.</small>
                    </div>
                </div>

                <h3 class="page-header">Database
                    <small>{params configuration}</small>
                </h3>

                <div class="form-group">
                    <label for="input_db_host" class="col-sm-2 control-label">Host</label>

                    <div class="col-sm-6">
                        <input class="form-control" id="input_db_host" placeholder="Host" name="db_host"
                               value="localhost">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Database Host.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="input_db_port" class="col-sm-2 control-label">Host</label>

                    <div class="col-sm-6">
                        <input class="form-control" id="input_db_port" placeholder="Port" name="db_port"
                               value="3306">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Database Port.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="input_db_name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-6">
                        <input class="form-control" id="input_db_name" placeholder="Name" name="db_name">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Database Name.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="input_db_user" class="col-sm-2 control-label">User</label>

                    <div class="col-sm-6">
                        <input class="form-control" id="input_db_user" placeholder="User" name="db_user">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Database User.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="input_db_password" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="input_db_password"
                               placeholder="Password" name="db_password">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Database Password.</small>
                    </div>
                </div>

                <h3 class="page-header">Security
                    <small>{}</small>
                </h3>

                <div class="form-group">
                    <label for="input_user" class="col-sm-2 control-label">User</label>

                    <div class="col-sm-6">
                        <input class="form-control" id="input_user" placeholder="User" name="user">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Administrator user.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="input_password" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="input_password"
                               placeholder="Password" name="password">
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <small class="help-block">Administrator Password.</small>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">

                        <hr>

                        <button type="submit" class="btn btn-default">
                            Next
                        </button>

                    </div>
                </div>

            </form>
        <?php } ?>

        <?php if ($section == 'finish') { ?>

            <h3 class="page-header">Finish
                <small>{we are ready !}</small>
            </h3>
            <p>You can change the config in the file <code>data/config.ini</code></p>
            <p>Visit <a href="http://ourapiserver.rabartu.info/api/">http://ourapiserver.rabartu.info/category/apis/</a>
                to get API's examples</p>
            <hr>
            <a href="<?php echo $_POST['url']; ?>/front/login" class="btn btn-default">
                Go Home
            </a>

        <?php } ?>

    </div>

</div>

</div>
<p class="text-muted pull-right" style="font-size:12px;margin-top: 40px;"><i
        class="fa fa-cogs"></i> <b>O</b>ur<b>A</b>pi<b>S</b>erver - 2014</p>

</div>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->

<script src="assets/js/bootstrap.min.js"></script>

</body>

</html>