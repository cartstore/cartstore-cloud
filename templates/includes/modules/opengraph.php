<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/
 

   
         if ($product_check['total'] > 0) {
          $product_info_query = tep_db_query("select p.products_id, pd.products_name as products_name, pd.products_description, p.products_image as products_image, p.products_price, p.products_quantity, p.products_tax_class_id, p.products_date_available from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$_GET['products_id'] . "' and p.products_status = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");

          if ( tep_db_num_rows($product_info_query) === 1 ) {
            $product_info = tep_db_fetch_array($product_info_query);

            $data = array('og:type' => 'product',
                          'og:title' => $product_info['products_name'],
                          'og:site_name' => STORE_NAME);

            if ( tep_not_null(MODULE_HEADER_TAGS_PRODUCT_OPENGRAPH_APP_ID) ) {
              $data['fb:app_id'] = MODULE_HEADER_TAGS_PRODUCT_OPENGRAPH_APP_ID;
            }

            $product_description = substr(trim(preg_replace('/\s\s+/', ' ', strip_tags($product_info['products_description']))), 0, 197);

            if ( strlen($product_description) == 197 ) {
              $product_description .= ' ..';
            }

            $data['og:description'] = $product_description;

            $products_image = $product_info['products_image'];

            $pi_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_info['products_id'] . "' order by products_id limit 1");

            if ( tep_db_num_rows($pi_query) === 1 ) {
              $pi = tep_db_fetch_array($pi_query);
              $products_image = $pi['image'];
            }

            $data['og:image'] = tep_href_link(DIR_WS_IMAGES . $product_info['products_image'], '', 'NONSSL', false, false);

            if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
              $products_price = $new_price;
            } else {
              $products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) ;
                
                  
            }

            $data['product:price:amount'] = $products_price;
            $data['product:price:currency'] = $currency;

            $data['og:url'] = tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product_info['products_id'], 'NONSSL', false);

            $data['product:availability'] = ( $product_info['products_quantity'] > 0 ) ? MODULE_HEADER_TAGS_PRODUCT_OPENGRAPH_TEXT_IN_STOCK : MODULE_HEADER_TAGS_PRODUCT_OPENGRAPH_TEXT_OUT_OF_STOCK;

            $result = '';

            foreach ( $data as $key => $value ) {
              $result .= '<meta property="' . tep_output_string_protected($key) . '" content="' . tep_output_string_protected($value) . '" />' . "\n";
            }
              echo $result;

           }
        }
    
 
      ?>
 

